export default class BookModel {
  constructor(
    title = '',
    description = '',
    image = '',
    author = '',
    rating = 0,
    price = '',
    pages = 0,
    buyLink = '',
  ) {
    this.title = title;
    this.description = description;
    this.image = image;
    this.authors = author;
    this.rating = rating;
    this.price = price;
    this.pages = pages;
    this.buyLink = buyLink;
  }
}
