import Bookservice from 'booksAppPixter/src/services/bookService';
import BookModel from 'booksAppPixter/src/models/bookModel';

export default class BookListModel {
  constructor() {
    this.books = [];
  }
  nextBooks() {
    return new Promise((resolve, reject) => {
      Bookservice.searchBooks('SEARCH_TERM', this.books.length).then(
        (response) => {
          for (let i in response.data.items) {
            this.books.push(
              new BookModel(
                response.data.items[i].volumeInfo.title,
                response.data.items[i].volumeInfo.description,
                response.data.items[i].volumeInfo.imageLinks.thumbnail ||
                  response.data.items[i].volumeInfo.imageLinks.smallThumbnail,
                response.data.items[i].volumeInfo.authors,
                response.data.items[i].volumeInfo.averageRating,
                response.data.items[i].saleInfo.listPrice &&
                  response.data.items[i].saleInfo.listPrice.amount,
                response.data.items[i].volumeInfo.pageCount,
                response.data.items[i].saleInfo.buyLink,
              ),
            );
          }
          resolve(this.books);
        },
      );
    });
  }

  refreshBooks() {
    return new Promise((resolve, reject) => {
      this.books = [];
      this.nextBooks().then(() => {
        resolve(this.books);
      });
    });
  }
}
