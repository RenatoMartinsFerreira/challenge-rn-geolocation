import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import fontelloConfig from 'booksAppPixter/src/config/selection.json';
export default createIconSetFromIcoMoon(fontelloConfig);
