export default {
  primary: '#FFDD0D',
  darkYellow: '#F0D10F',
  subtitle: '#9F8B0C',
  lightBlue: '#4A90E2',
  hearthRed: '#DC4B5D',
  midnightBlack: '#2C2605',
  brown: '#4C4309',
  lightBrown: '#E4C81B',
  babyBlue: '#4A90E2',
  dark: '#4F565D',
  white: '#FFFFFF',
};
