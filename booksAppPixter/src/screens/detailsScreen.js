import React, {Component} from 'react';
import {
  View,
  Image,
  SafeAreaView,
  StatusBar,
  ScrollView,
  Linking,
  StyleSheet,
} from 'react-native';
import {
  GenericTextComponent,
  AppHeaderComponent,
  GenericButtomComponent,
} from 'booksAppPixter/src/components/presentation';
import colors from 'booksAppPixter/src/commons/colors';
import {RatingComponent} from 'booksAppPixter/src/components/container';
import {verticalScale, horizontalScale, fontScale} from '../commons/scaling';
import Icon from 'booksAppPixter/src/commons/icon';

export class DetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;
    this.book = this.props.route.params.item;

    this.state = {
      favorite: false,
      rating: 0,
    };
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={colors.primary} />

        <AppHeaderComponent
          title={'Pixter Book'}
          onBack={() => {
            this.navigation.pop();
          }}
        />

        <View style={styles.content}>
          <View style={styles.topContent}>
            <View style={styles.topLeftContent}>
              <View style={[styles.shadowImageContainer]}>
                <Image
                  style={styles.bookImage}
                  resizeMode="stretch"
                  source={{
                    uri: this.book.image,
                  }}
                />
              </View>
              <View style={styles.pagesView}>
                {this.book.pages && (
                  <GenericTextComponent
                    color={colors.subtitle}
                    text={`${this.book.pages} pages`}
                  />
                )}
              </View>
            </View>
            <View style={styles.topRightContent}>
              <GenericTextComponent
                text={this.book.title}
                styleguideItem={GenericTextComponent.StyleguideItem.HEADING}
              />
              <View style={styles.authorView}>
                <GenericTextComponent
                  style={styles.authorView}
                  color={colors.subtitle}
                  text={`by ${this.book.authors[0]}`}
                />
              </View>

              <View style={styles.starsContainer}>
                {!!this.book.price && (
                  <View style={styles.priceView}>
                    <GenericTextComponent
                      styleguideItem={
                        GenericTextComponent.StyleguideItem.HEADING
                      }
                      text={`$${this.book.price}`}
                    />
                  </View>
                )}
                <RatingComponent
                  rating={this.state.rating}
                  onChangeRating={(rate) => {
                    console.log('rating', rate);

                    this.setState({rating: rate});
                  }}
                />
              </View>

              <View style={styles.buttonsContainer}>
                <GenericButtomComponent
                  onPress={() =>
                    this.book.buyLink &&
                    Linking.openURL(this.book.buyLink).catch((err) =>
                      console.error("Couldn't load page", err),
                    )
                  }
                  style={styles.shadowBuyContainer}
                  color={colors.babyBlue}
                  children={
                    <View
                      style={{
                        paddingVertical: fontScale(6),
                        marginHorizontal: horizontalScale(40),
                      }}>
                      <GenericTextComponent
                        styleguideItem={
                          GenericTextComponent.StyleguideItem.BUTTONLABEL
                        }
                        color={colors.white}
                        text={'Buy'}
                      />
                    </View>
                  }
                />
                <View
                  style={{
                    margin: horizontalScale(9),
                    aspectRatio: 1,
                  }}>
                  <GenericButtomComponent
                    onPress={() => {
                      this.setState({favorite: !this.state.favorite});
                    }}
                    color={colors.hearthRed}
                    children={
                      <View
                        style={{
                          margin: horizontalScale(9),
                        }}>
                        <Icon
                          name={this.state.favorite ? 'heartFullFill' : 'heart'}
                          size={fontScale(15)}
                          color={colors.white}
                        />
                      </View>
                    }
                  />
                </View>
              </View>
            </View>
          </View>

          <ScrollView
            contentContainerStyle={{paddingVertical: verticalScale(31)}}
            style={styles.bottomContent}>
            <GenericTextComponent
              styleguideItem={GenericTextComponent.StyleguideItem.BODY}
              text={this.book.description}
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    alignSelf: 'stretch',
    flex: 1,
    flexDirection: 'column',
    paddingTop: verticalScale(19),
  },
  topContent: {
    flex: 0.55,
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: horizontalScale(18),
  },
  topLeftContent: {
    paddingTop: verticalScale(5),
    marginRight: horizontalScale(23),
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  topRightContent: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  pagesView: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingBottom: verticalScale(5),
  },

  bottomContent: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: horizontalScale(17),
  },

  starsContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: horizontalScale(10),
  },
  priceView: {
    marginRight: horizontalScale(9),
  },

  shadowImageContainer: {
    flex: 2,
    alignSelf: 'stretch',
    aspectRatio: 10 / 13,
    backgroundColor: colors.primary,
    elevation: 33,

    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowOpacity: 0.8,
    shadowRadius: 20,
    shadowColor: 'rgba(184, 118, 12,0.725119)',
  },

  shadowBuyContainer: {
    backgroundColor: colors.primary,
    elevation: 33,

    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    shadowColor: 'rgba(60, 120, 191,0.4926)',
  },

  bookImage: {
    flex: 1,
  },
});
export default DetailsScreen;
