import React, {Component} from 'react';
import {
  Image,
  FlatList,
  StyleSheet,
  TouchableHighlight,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import colors from 'booksAppPixter/src/commons/colors';
import BookListModel from 'booksAppPixter/src/models/bookListModel';
import {AppHeaderComponent} from 'booksAppPixter/src/components/presentation';
import {verticalScale, horizontalScale} from '../commons/scaling';

export class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;
    this.bookListModel = new BookListModel();

    this.state = {
      bookList: [],
      refreshing: true,
    };
  }

  componentDidMount() {
    this.bookListModel.nextBooks().then((list) => {
      this.setState({bookList: list, refreshing: false});
    });
  }

  refreshList() {
    this.setState({refreshing: true});
    this.bookListModel.refreshBooks().then((list) => {
      this.setState({bookList: list, refreshing: false});
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={colors.primary} />
        <AppHeaderComponent title={'Pixter Books'} onSearch={() => {}} />
        <FlatList
          style={styles.flatListBooksGrid}
          data={this.state.bookList}
          extraData={this.state.bookList}
          showsVerticalScrollIndicator={false}
          onEndReached={() => {
            this.bookListModel.nextBooks().then((list) => {
              this.setState({bookList: list});
            });
          }}
          onRefresh={() => {
            this.refreshList();
          }}
          refreshing={this.state.refreshing}
          renderItem={({item, index}) => (
            <TouchableHighlight
              key={index}
              style={styles.bookItem}
              onPress={() => {
                this.navigation.navigate('Details', {
                  item,
                });
              }}>
              <Image
                style={{flex: 1}}
                source={{
                  uri: item.image,
                }}
              />
            </TouchableHighlight>
          )}
          numColumns={3}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flatListBooksGrid: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    marginTop: verticalScale(25),
    marginHorizontal: verticalScale(8),
  },
  bookItem: {
    flex: 1,
    aspectRatio: 10 / 13,
    margin: horizontalScale(10),
  },
});

export default HomeScreen;
