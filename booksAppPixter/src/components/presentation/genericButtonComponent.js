import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

export const GenericButtomComponent = ({color, children, onPress, style}) => {
  return (
    <TouchableOpacity
      onPress={() => onPress()}
      style={[styles.container, style, {backgroundColor: color}]}>
      {children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'green',
    borderRadius: 9999,
  },
});
