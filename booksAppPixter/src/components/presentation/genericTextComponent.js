import React from 'react';
import {StyleSheet, Text} from 'react-native';
import PropTypes from 'prop-types';
import colors from 'booksAppPixter/src/commons/colors';
import {fontScale} from 'booksAppPixter/src/commons/scaling';

export const StyleguideItem = {
  HEADINGTHIN: 'HEADINGTHIN',
  HEADING: 'HEADING',
  BODY: 'BODY',
  BUTTONLABEL: 'BUTTONLABEL',
  DEFAULT: 'DEFAULT',
};

export const GenericTextComponent = ({
  styleguideItem,
  text,
  color,
  opacity,
  textAlign,
  marginTop,
  marginBottom,
  numberOfLines,
  strike,
}) => {
  let currentStyle;

  switch (styleguideItem) {
    case StyleguideItem.HEADING:
      currentStyle = styles.heading;
      break;
    case StyleguideItem.HEADINGTHIN:
      currentStyle = styles.headingThin;
      break;
    case StyleguideItem.BODY:
      currentStyle = styles.body;
      break;

    case StyleguideItem.BUTTONLABEL:
      currentStyle = styles.buttonLabel;
      break;

    default:
      currentStyle = styles.default;
      break;
  }

  return (
    <Text
      style={[
        currentStyle,
        {
          opacity,
          color,
          textAlign,
          marginTop,
          marginBottom,
        },
        !!strike && styles.strike,
      ]}
      allowFontScaling={false}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

GenericTextComponent.defaultProps = {
  styleguideItem: StyleguideItem.DEFAULT,
  color: colors.black,
  opacity: 1,
  textAlign: 'left',
  marginTop: 0,
  marginBottom: 0,
  numberOfLines: 99999,
  strike: false,
};

GenericTextComponent.propTypes = {
  styleguideItem: PropTypes.oneOf(Object.keys(StyleguideItem)),
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
  opacity: PropTypes.number,
  textAlign: PropTypes.string,
  marginTop: PropTypes.number,
  marginBottom: PropTypes.number,
  numberOfLines: PropTypes.number,
  strike: PropTypes.bool,
};

GenericTextComponent.StyleguideItem = StyleguideItem;

const styles = StyleSheet.create({
  heading: {
    fontSize: fontScale(20),
    fontWeight: 'bold',
  },
  headingThin: {
    fontSize: fontScale(20),
  },
  body: {
    fontSize: fontScale(14),
    lineHeight: fontScale(30),
  },
  buttonLabel: {
    fontSize: fontScale(14),
    lineHeight: fontScale(24),
    fontWeight: 'bold',
  },
  default: {
    fontSize: fontScale(14),
    lineHeight: fontScale(24),
  },
});
