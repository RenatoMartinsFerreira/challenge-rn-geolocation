import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {verticalScale, fontScale} from 'booksAppPixter/src/commons/scaling';
import {GenericTextComponent} from 'booksAppPixter/src/components/presentation/index.js';
import colors from 'booksAppPixter/src/commons/colors';
import Icon from 'booksAppPixter/src/commons/icon';

export const AppHeaderComponent = ({title, onSearch = null, onBack = null}) => {
  return (
    <View style={styles.container}>
      <View style={styles.leftCorner}>
        {onBack && (
          <TouchableOpacity
            onPress={() => {
              onBack();
            }}>
            <Icon
              name="arrowb"
              size={fontScale(15)}
              color={colors.midnightBlack}
              style={{paddingLeft: verticalScale(17)}}
            />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.center}>
        <GenericTextComponent
          text={title}
          color={colors.midnightBlack}
          styleguideItem={GenericTextComponent.StyleguideItem.HEADINGTHIN}
        />
      </View>
      <View style={styles.rightCorner}>
        {onSearch && (
          <TouchableOpacity
            onPress={() => {
              onSearch();
            }}>
            <Icon
              name="search"
              size={fontScale(20)}
              color={colors.midnightBlack}
              style={{paddingRight: verticalScale(17)}}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: verticalScale(66),
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: verticalScale(8),
  },
  leftCorner: {
    flex: 1,
    borderBottomWidth: verticalScale(2),
    borderColor: colors.primary,
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  center: {
    flex: 1,
    flexBasis: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    borderBottomColor: colors.darkYellow,
    borderBottomWidth: verticalScale(2),
  },
  rightCorner: {
    flex: 1,
    borderBottomWidth: verticalScale(2),
    borderColor: colors.primary,
    alignItems: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'column',
  },
});
