import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import { fontScale} from 'booksAppPixter/src/commons/scaling';
import colors from 'booksAppPixter/src/commons/colors';
import Icon from 'booksAppPixter/src/commons/icon';

export class RatingComponent extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        {this.ratingPupulation(this.props.rating)}
      </View>
    );
  }

  ratingPupulation = (rating) => {
    let stars = [];

    const reviews = ['Bad', 'OK', 'Good', 'Very Good', 'Amazing'];

    for (let review in reviews) {
      stars.push(
        <TouchableOpacity
          onPress={() => {
            this.props.onChangeRating(review + 1);
          }}>
          <Icon
            name="Page"
            size={fontScale(17)}
            color={review < rating ? colors.brown : colors.lightBrown}
            style={{marginLeft: fontScale(3)}}
          />
        </TouchableOpacity>,
      );
    }

    return <View style={{flexDirection: 'row'}}>{stars}</View>;
  };
}

const styles = StyleSheet.create({container: {flexDirection: 'row'}});
