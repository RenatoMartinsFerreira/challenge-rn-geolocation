import axios from 'axios';

export default {
  searchBooks: (term = 'default', startIndex = 0, maxResult = 12) =>
    axios.get(
      `https://www.googleapis.com/books/v1/volumes?q=${term}&orderBy=relevance&startIndex=${startIndex}&maxResults=${maxResult}`,
    ),
};
